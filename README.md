#REST Microservice example

The service gererates the GET request for create list of colours like below:

````
{
    DARK: [
        "BLACK",
        "DARKBLUE",
        "DARKGREEN",
        "POMEGRANATE"
    ],
    BRIGHT: [
        "GREEN",
        "PINK",
        "SILVER",
        "WHITE",
        "YELLOW"
    ]
}