package com.adamlesiak.codeexample.repository;

import com.adamlesiak.codeexample.data.ColourData;
import com.adamlesiak.codeexample.enums.Colour;
import com.adamlesiak.codeexample.enums.ColourType;
import lombok.Data;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Data
@Repository
public class ColoursListRepository {

    private List<ColourData> data;

    @PostConstruct
    private void generateList() {
        data =  Arrays.asList(
                ColourData.builder().colourType(ColourType.BRIGHT).colour(Colour.WHITE).build(),
                ColourData.builder().colourType(ColourType.BRIGHT).colour(Colour.YELLOW).build(),
                ColourData.builder().colourType(ColourType.BRIGHT).colour(Colour.SILVER).build(),
                ColourData.builder().colourType(ColourType.BRIGHT).colour(Colour.GREEN).build(),
                ColourData.builder().colourType(ColourType.BRIGHT).colour(Colour.PINK).build(),

                ColourData.builder().colourType(ColourType.DARK).colour(Colour.POMEGRANATE).build(),
                ColourData.builder().colourType(ColourType.DARK).colour(Colour.DARKGREEN).build(),
                ColourData.builder().colourType(ColourType.DARK).colour(Colour.DARKBLUE).build(),
                ColourData.builder().colourType(ColourType.DARK).colour(Colour.BLACK).build()
        );
    }

}
