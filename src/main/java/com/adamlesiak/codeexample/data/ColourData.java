package com.adamlesiak.codeexample.data;

import com.adamlesiak.codeexample.enums.Colour;
import com.adamlesiak.codeexample.enums.ColourType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ColourData {
    private ColourType colourType;
    private Colour colour;
}
