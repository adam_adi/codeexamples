package com.adamlesiak.codeexample.service;


import com.adamlesiak.codeexample.data.ColourData;
import com.adamlesiak.codeexample.enums.Colour;
import com.adamlesiak.codeexample.enums.ColourType;
import com.adamlesiak.codeexample.repository.ColoursListRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class TestController {

    private ColoursListRepository coloursListRepository;

    public TestController(ColoursListRepository coloursListRepository) {
        this.coloursListRepository = coloursListRepository;
    }

    @GetMapping(value = "/get-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<ColourType, List<Colour>> getAll() {

        return coloursListRepository.getData()
                .stream()
                .sorted(Comparator.comparing(e -> e.getColour().toString()))
                .collect(Collectors.groupingBy(ColourData::getColourType, Collectors.mapping(ColourData::getColour, Collectors.toList())));
    }

    @GetMapping(value = "/get-data/{startsWith}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<ColourType, List<Colour>> getByStartsWith(@PathVariable (value = "startsWith") String _startsWith) {

        Optional<String> startsWith = Optional.ofNullable(_startsWith);

        return coloursListRepository.getData()
                .stream()
                .filter(e -> e.getColour().toString().toLowerCase().startsWith(startsWith.orElse("").toLowerCase()))
                .sorted(Comparator.comparing(e -> e.getColour().toString()))
                .collect(Collectors.groupingBy(ColourData::getColourType, Collectors.mapping(ColourData::getColour, Collectors.toList())));
    }


}
