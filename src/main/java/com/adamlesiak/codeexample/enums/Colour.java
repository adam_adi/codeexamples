package com.adamlesiak.codeexample.enums;

public enum Colour {
    WHITE, YELLOW, SILVER, GREEN, DARKGREEN, DARKBLUE, BLACK, POMEGRANATE, PINK
}
